package main

import (
	"bitbucket.org/fasales/colly-fasthttp-v2"
	"bitbucket.org/fasales/colly-fasthttp-v2/queue"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"
)

func ToRawCookies(cookies []http.Cookie) string {
	str := ""
	items := len(cookies) - 1
	for idx, row := range cookies {
		str += row.String()
		if idx < items {
			str += "; "
		}
	}
	return str
}

func RemoveEmptyHeaders(headers [][2]string) [][2]string {
	newList := make([][2]string, 0, len(headers))
	for _, header := range headers {
		if header[1] != "" {
			newList = append(newList, header)
		}
	}
	return newList
}

func SetCookiesAndHeaders(c *colly.Collector, cookies []http.Cookie, headers [][2]string) {
	c.OnResponse(func(response *colly.Response) {
		response.Ctx.Put("referer", response.Request.URL.String())
	})

	c.OnRequest(func(request *colly.Request) {
		for idx, header := range headers {
			switch header[0] {
			case "Host":
				headers[idx][1] = request.URL.Host
				break
			case "Referer":
				if referer := request.Ctx.Get("referer"); referer != "" {
					headers[idx][1] = referer
				}
				break
			case "Cookie":
				if cookies != nil {
					headers[idx][1] = ToRawCookies(cookies)
				}
				break
			}
		}

		activeHeaders := RemoveEmptyHeaders(headers)
		request.Ctx.Put("headers", activeHeaders)

		for _, header := range activeHeaders {
			log.Println(header[0] + "=" + header[1])
		}
	})
}

func main() {
	c := colly.NewCollector(
		// Visit only domains: hackerspaces.org, wiki.hackerspaces.org
		colly.AllowedDomains("www.antonioli.eu", "www.allsole.com"),
	)

	limit := &queue.Limit{
		RandomDelay: &queue.RandomDelay{
			Min: time.Second,
			Max: time.Second * 5,
		},
	}

	q, err := queue.New(1, limit, &queue.InMemoryQueueStorage{MaxSize: 10000})
	if err != nil {
		log.Fatalln("Error:", err.Error())
	}

	cookies := []http.Cookie{
		//{Name: "dtSa", Value: "-"},
		//{Name: "ipp_uid_tst", Value: "1576279228652/JscDez0MdgcGBYigUcFNOA"},
		//{Name: "rerf", Value: "AAAAAF30HLwGQZzVAxAlAg"},
		//{Name: "ipp_key", Value: "v1576279228679/v3394bd8718c1ac5c02cf2c163aeca6afa04ab3/TpQWr6qWRdi/QBaZf1Myvw"},
		//{Name: "ipp_uid2", Value: "7w003uxNL8e8wcqc/8vQ4Ii7A8tdJJrtqPow5EQ"},
		//{Name: "ipp_uid1", Value: "1576279228679"},
		//{Name: "ipp_uid", Value: "1576279228679/7w003uxNL8e8wcqc/8vQ4Ii7A8tdJJrtqPow5EQ"},
		//{Name: "dtCookie", Value: "-4$O27EKU3DF3AQV6COB7J58GUS00F46FM6"},
		//{Name: "rxVisitor", Value: "157627922943839UI5BQNCQIU4VGM0QT6B5AMI44U7E4A"},
		//{Name: "guest_token", Value: "Im5JSXhFdkkwT0hLQ0tISEJNQjBzaHci--d7b3f0c285d98b2d4996bff8d99c516f03c4f098"},
		//{Name: "_pk_ses.3.feaa", Value: "*"},
		//{Name: "dismiss_cookie_law", Value: "true"},
		//{Name: "_fbp", Value: "fb.1.1576279229742.848871189"},
		//{Name: "_hjid", Value: "bb6681c8-85c5-48c8-9a5d-48e780b39110"},
		//{Name: "dtLatC", Value: "90"},
		//{Name: "_pk_id.3.feaa", Value: "cb1f9b8646d2d017.1576279230.1.1576279310.1576279230."},
		//{Name: "dtPC", Value: "-4$79309813_177h-vWHALRUKOMKNUHHOQMFVMKNEZDJPBBIXB"},
		//{Name: "rxvt", Value: "1576281116023|1576279229439"},
	}

	headers := [][2]string{
		{"Host", ""},
		{"Connection", "keep-alive"},
		{"Pragma", "no-cache"},
		{"Cache-Control", "no-cache"},
		{"Upgrade-Insecure-Requests", "1"},
		{"User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36 OPR/65.0.3467.62"},
		{"Sec-Fetch-User", "?1"},
		{"Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"},
		{"Sec-Fetch-Site", "none"},
		{"Sec-Fetch-Mode", "navigate"},
		{"Referer", ""},
		{"Accept-Encoding", "gzip, deflate, br"},
		{"Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6,es;q=0.5,de;q=0.4"},
		//{"dnt", "1"},
		{"Cookie", ""},
	}

	SetCookiesAndHeaders(c, cookies, headers)

	//err = c.Limit(&colly.LimitRule{
	//	Delay:       time.Second * 3,
	//	Parallelism: 1,
	//})
	//if err != nil {
	//	log.Println("Error:", err.Error())
	//}

	// On every a element which has href attribute call callback
	c.OnHTML(`a[itemprop="url"]`, func(e *colly.HTMLElement) {
		link := e.Attr("href")
		// Print link
		fmt.Printf("Link found: %q -> %s\n", e.Text, link)
		// Visit link found on page
		// Only those links are visited which are in AllowedDomains
		//c.Visit(e.Request.AbsoluteURL(link))

		//ctx := colly.NewContext()
		//ctx.Put("referrer", e.Request.URL.String())

		absolute, err := url.Parse(e.Request.AbsoluteURL(link))
		if err != nil {
			log.Println("Error:", err.Error())
		}

		err = q.AddRequest(&colly.Request{
			URL:    absolute,
			Ctx:    e.Request.Ctx,
			Method: "GET",
		})
		if err != nil {
			log.Println("Error:", err.Error())
		}

		//c.Request("GET", e.Request.AbsoluteURL(link), nil, ctx, nil)
	})

	c.OnResponse(func(response *colly.Response) {
		log.Println("Response status:", response.StatusCode, string(response.Body), response.Headers)
	})

	c.OnError(func(response *colly.Response, e error) {
		log.Println("Response status:", response.StatusCode, response.StatusCode, string(response.Body), response.Headers)
	})

	//c.Visit("https://www.antonioli.eu/en/BY/men/section/clothing")
	//if err := q.AddURL("https://www.allsole.com/catalogue/women/footwear.list?facetFilters=en_FootwearType_content:Flats%7Cen_brand_content:Clarks+Originals%7Cen_brand_content:Converse%7Cen_brand_content:Dr.+Martens%7Cen_brand_content:Converse%7Cen_brand_content:Dr.+Martens%7Cen_brand_content:Converse%7Cen_brand_content:Dr.+Martens%7Cen_brand_content:Emporio+Armani%7Cen_brand_content:Puma%7Cen_brand_content:Polo+Ralph+Lauren%7Cen_brand_content:Saucony%7Cen_brand_content:Tommy+Hilfiger%7Cen_brand_content:Vans"); err != nil {
	if err := q.AddURL("https://www.allsole.com/catalogue/women/footwear.list"); err != nil {
		log.Println("Error:", err.Error())
	}

	if err := q.Run(c); err != nil {
		log.Println("Error:", err.Error())
	}

}
